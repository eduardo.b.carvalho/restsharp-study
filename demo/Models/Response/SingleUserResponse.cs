﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIDemo.Models.Response
{
    
    public class SingleUserResponse
    {
        public Data data { get; set; }
        public Support support { get; set; }
    }

       
}
