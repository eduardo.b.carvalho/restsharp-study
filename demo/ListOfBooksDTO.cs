﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIDemo
{
    public partial class ListOfBooksDTO
    {
            public Book[] Books { get; set; }
        }

        public partial class Book
        {
            public string Isbn { get; set; }
            public string Title { get; set; }
            public string SubTitle { get; set; }
            public string Author { get; set; }
            public DateTimeOffset PublishDate { get; set; }
            public string Publisher { get; set; }
            public long Pages { get; set; }
            public string Description { get; set; }
            public Uri Website { get; set; }
        }
}

