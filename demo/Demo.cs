﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIDemo
{
    public class Demo
    {
        public ListOfBooksDTO GetBooks()
        {
            var restClient = new RestClient("https://demoqa.com");
            var restRequest = new RestRequest("/BookStore/v1/Books", Method.Get);
            restRequest.AddHeader("Content-Type", "application/json");
            restRequest.RequestFormat = DataFormat.Json;

            RestResponse response = restClient.Execute(restRequest);
            var content = response.Content;

            var books = JsonConvert.DeserializeObject<ListOfBooksDTO>(content);
            return books;
        }
    }
}
