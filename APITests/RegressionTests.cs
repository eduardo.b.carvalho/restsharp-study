﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using APIDemo;

namespace APITests
{
    [TestClass]
    public class RegressionTests
    {
        [TestMethod]
        public void VerifyListOfBooks()
        {
            var demo = new Demo();
            var response = demo.GetBooks();
            Assert.AreEqual("Git Pocket Guide", response.Books[0].Title);
            
        }
        [TestMethod]
        public void TestMethod2()
        {

        }
    }
}
