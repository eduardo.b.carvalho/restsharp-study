﻿Feature: CreateUser
	Create a new user using API
@test_api
Scenario: Create a new user with valid inputs
	Given User with name "Peter"
	And user with job "Manager"
	When Send request to create user
	Then Validate user is created

Scenario: Create a new user with valid inputs from a saved file
	Given User payload "CreateUSer.json"
	When Send request to create user
	Then Validate user is created