using APIDemo;
using APIDemo.Models.Request;
using APIDemo.Models.Response;
using APIDemo.Utility;
using NUnit.Framework;
using RestSharp;
using System;
using System.Net;
using TechTalk.SpecFlow;

namespace testes.Features
{
    [Binding]
    public class CreateUserSteps
    {
        private CreateUserRequest createUserRequest;
        private RestResponse response;
        private ScenarioContext scenarioContext;
        private HttpStatusCode statusCode;
        private APIClient api;

        public CreateUserSteps(CreateUserRequest createUserRequest, ScenarioContext scenarioContext)
        {
            this.scenarioContext = scenarioContext;
            this.createUserRequest = createUserRequest;
            api = new APIClient();
        }

        [Given(@"User with name ""([^""]*)""")]
        public void GivenUserWithName(string name)
        {
            createUserRequest.name = name;
        }

        [Given(@"user with job ""([^""]*)""")]
        public void GivenUserWithJob(string job)
        {
            createUserRequest.job = job;
        }

        [Given(@"User payload ""([^""]*)""")]
        public void GivenUserPayload(string filename)
        {
            string file = HandleContent.GetFilePath(filename);
            var payload = HandleContent.ParseJson<CreateUserRequest>(file);
            scenarioContext.Add("create_user_payload", payload);
        }


        [When(@"Send request to create user")]
        public async Task WhenSendRequestToCreateUser()
        {
            createUserRequest = scenarioContext.Get<CreateUserRequest>("create_user_payload");
            response = await api.CreateUser<CreateUserRequest>(createUserRequest);
        }

        [Then(@"Validate user is created")]
        public void ThenValidateUserIsCreated()
        {
            statusCode = response.StatusCode;
            var code = (int)statusCode;
            //Assert.AreEqual(201, code);
            Assert.That(code, Is.EqualTo(201));

            var content = HandleContent.GetContent<CreateUserResponse>(response);
            //Assert.AreEqual(createUserRequest.name, content.name);
            Assert.That(createUserRequest.name, Is.EqualTo(content.name));
            //Assert.AreEqual(createUserRequest.job, content.job);
            Assert.That(createUserRequest.job, Is.EqualTo(content.job));
        }
    }
}
